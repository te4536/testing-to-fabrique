import os

from django.core.mail import send_mail
from django.utils.timezone import now

import requests

import api
from djangoProject.celery import app
import datetime
import logging

import api.models
from api.selectors import parse_query


@app.task(bind=True)
def process_mailing(self, **kwargs):
    url = 'https://probe.fbrq.cloud/v1/send/{0}'
    headers = {"Authorization": "Bearer " + os.environ.get("API_TOKEN")}
    mailing_id = kwargs.get("mailing_id")
    logging.info(f"Process mailing({datetime.datetime.now()}): {mailing_id}")
    if api.models.Mailing.objects.filter(pk=mailing_id).count() != 0:
        mailing = api.models.Mailing.objects.get(pk=mailing_id)
        for client in api.models.Client.objects.filter(parse_query(mailing.filter)):
            send_message.apply_async(kwargs={'client_id': client.id, 'url': url, 'headers': headers, 'client_phone': client.phone,
                                     'text': mailing.text, 'mailing_id': mailing_id})


@app.task(bind=True, default_retry_delay=60, max_retries=5)
def send_message(self, **kwargs):
    logging.info(f"Running sending message to client: {kwargs.get('client_phone')}")
    if kwargs.get("msg_id", "") != "":
        message = api.models.Message.objects.get(pk=kwargs.get("msg_id"))
    else:
        if api.models.Mailing.objects.filter(pk=kwargs.get("mailing_id")).count() != 0:
            mailing = api.models.Mailing.objects.get(pk=kwargs.get("mailing_id"))
            client = api.models.Client.objects.get(pk=kwargs.get("client_id"))
            message = api.models.Message.objects.create(sent=now(),
                                                        status=api.models.Message.Status.ON_HOLD,
                                                        mailing=mailing, client=client)
            message.save()
            kwargs["msg_id"] = message.id
        else:
            self.retry()
            return
    try:
        if api.models.Mailing.objects.get(pk=kwargs.get("mailing_id")).end.timestamp() < now().timestamp():
            logging.warning(f"Message to {kwargs.get('client_phone')} was late")
            message.status = api.models.Message.Status.LATE
            message.save()
        else:
            res = requests.post(url=kwargs.get("url").format(message.client_id), headers=kwargs.get("headers"),
                                json={'id': message.client_id, 'phone': message.client.phone,
                                      'text': kwargs.get("text")})

            if res.status_code == 200 and res.json()["code"] == 0:
                logging.info(f"Message to {message.client.phone} was sent!")
                message.status = api.models.Message.Status.SENT
                message.save()
            else:
                logging.error(f"Message for {message.client.phone} was not sent: error {res.status_code}")
                message.status = api.models.Message.Status.ON_HOLD
                message.save()
                raise self.retry()

    except Exception as e:
        logging.error(f"Unexpected error while sending to {message.client.phone}: {str(e)}")
        raise self.retry(exc=e)


@app.on_after_configure.connect
def period_statistics(sender, **kwargs):
    sender.add_periodic_task(24*60*60, send_statistics.s())


@app.task
def send_statistics():
    mails = list(api.models.Mailing.objects.filter(message__status=api.models.Message.Status.SENT)) + \
           list(api.models.Mailing.objects.filter(message__status=api.models.Message.Status.ON_HOLD)) + \
           list(api.models.Mailing.objects.filter(message__status=api.models.Message.Status.LATE))
    send_mail(
        f"Everyday statistics on {now().date()}",
        f"{api.serializers.StatisticsSerializer(mails).data}",
        f'{os.environ.get("FROM_EMAIL")}',
        [f'{os.environ.get("TO_EMAIL")}'],
        fail_silently=False
    )
