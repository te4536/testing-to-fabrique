from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register(r'client', views.ClientViewSet)
router.register(r'mailing', views.MailingViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('stat/<int:pk>/', views.MailingDetailStatistics.as_view()),
    path('stat/', views.MailingStatistics.as_view()),
]

