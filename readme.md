# Тестовое задание в Фабрику Решений
![Logo of company](./logo.jpg)
## REST Api for Mailing system<br>
## Additional tasks:<br>
1. Подготовить docker-compose для запуска всех сервисов проекта одной командой
2. Cделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API.
3. Удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
4. Обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах всю информацию.<br>
### API Endpoints:
- /docs
    - GET - get docs in Swagger Format
- /api
    - /client
      - GET - get all clients in system
      - POST - create new client
      - /{pk:int}
        - GET - get detail client with id
        - PUT - replace client with this id
        - PATCH - modify existing client
        - DELETE - delete Client
    - /mailing
      - GET - get all mailing in system
      - POST - create new mailing
      - /{pk:int}
        - GET - get detail mailing entity with id
        - PUT - replace mailing with this id
        - PATCH - modify existing mailing
        - DELETE - delete mailing
    - /stat
      - GET - get common statistics about mailings grouped in statuses
      - /{pk:int}
        - GET - get all sent messages by existing mailing
### Some explanations
1. djangoProject is main app with only config and etc.
2. api is app containing all logic, models and etc.
3. tasks.py containing task to process just mailing entity and the next func send_message will run for every client and checks all conditions and store statistics.
### Running
You need to fill .env file according to .env.example and then run:

        sudo docker-compose up --build
