from django.db import models
import pytz
import datetime

import api.tasks

TIMEZONES = tuple(zip(pytz.common_timezones, pytz.common_timezones))


class Mailing(models.Model):
    start = models.DateTimeField()
    text = models.TextField()
    # example of filter: operator=927&tag=people
    filter = models.CharField(max_length=300, default="")
    end = models.DateTimeField()

    def save(self, *args, **kwargs):
        super(Mailing, self).save(*args, **kwargs)
        if datetime.datetime.now().timestamp() > self.start.timestamp():
            api.tasks.process_mailing.delay(mailing_id=self.id)
        else:
            api.tasks.process_mailing.apply_async(kwargs={"mailing_id": self.id},
                                                  countdown=(self.start - datetime.datetime.now()).total_seconds())

    @property
    def total(self):
        return self.message_set.count()


class Client(models.Model):
    phone = models.CharField(max_length=11)
    operator = models.CharField(max_length=3)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')

    def __str__(self):
        return self.phone


class Message(models.Model):
    class Status(models.IntegerChoices):
        SENT = 2
        ON_HOLD = 1
        LATE = 0

    sent = models.DateTimeField()
    status = models.IntegerField(choices=Status.choices)
    mailing = models.ForeignKey(to=Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(to=Client, on_delete=models.CASCADE)
