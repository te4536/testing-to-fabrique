from rest_framework import serializers

from api.models import Client, Mailing, Message


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['phone', 'operator', 'tag', 'timezone']


class MailingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mailing
        fields = ['start', 'text', 'filter', 'end']


class MessageSerializer(serializers.ModelSerializer):
    client = ClientSerializer()

    class Meta:
        model = Message
        fields = ['sent', 'status', 'client']


class StatisticsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mailing
        fields = ['start', 'text', 'filter', 'end', 'total']
