from django.contrib import admin

from api.models import Client, Message, Mailing

admin.site.register(Client)
admin.site.register(Message)
admin.site.register(Mailing)
