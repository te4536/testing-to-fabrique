import re

from django.db.models import Q

import api.models


def parse_query(query: str) -> Q:
    parsed = re.split(r'[=&|]', query)
    kwargs = {parsed[0]: parsed[1]}
    if len(parsed) == 2:
        return Q(**kwargs)
    elif len(parsed) == 4:
        if query.find("&") != -1:
            return Q(**kwargs) & Q(**{parsed[2]: parsed[4]})
        elif query.find("|") != -1:
            return Q(**kwargs) | Q(**{parsed[2]: parsed[4]})
    return Q()


def get_filter(mailing_id):
    return api.models.Mailing.objects.get(pk=mailing_id).filter


def get_text(mailing_id):
    return api.models.Mailing.objects.get(mailing_id).text
