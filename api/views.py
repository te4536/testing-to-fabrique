import rest_framework.viewsets
from django.db.models import Count
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Client, Mailing, Message
from api.serializers import ClientSerializer, MailingSerializer, MessageSerializer, StatisticsSerializer


class ClientViewSet(rest_framework.viewsets.ModelViewSet):
    """
    CRUD for Client entity
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.AllowAny]


class MailingViewSet(rest_framework.viewsets.ModelViewSet):
    """
    CRUD for Mailing entity
    """
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    permission_classes = [permissions.AllowAny]


class MailingDetailStatistics(APIView):
    """
    Detail statistics for mailing entity
    """
    permission_classes = []

    def get(self, request, pk):
        mail = Mailing.objects.get(pk=pk)
        messages = mail.message_set.all()
        serializer = MessageSerializer(list(messages), many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class MailingStatistics(APIView):
    """
    Common statistics for all mailing with grouped counts of messages
    """
    def get(self, request):
        mails_sent = list(Mailing.objects.filter(message__status=Message.Status.SENT))
        mails_hold = list(Mailing.objects.filter(message__status=Message.Status.ON_HOLD))
        mails_late = list(Mailing.objects.filter(message__status=Message.Status.LATE))
        return Response({"SENT": StatisticsSerializer(mails_sent, many=True).data,
                         "ON_HOLD": StatisticsSerializer(mails_hold, many=True).data,
                         "LATE": StatisticsSerializer(mails_late, many=True).data}, status=status.HTTP_200_OK)

